## Installation

```shell
yarn install
# or
npm install
```

###  Running local development server

```shell
yarn run serve
# or
npm run serve
```

# Generate IOS App

```shell
npm run build
```

Delete IOS folder if exists

```shell
npx cap add ios
npx cap open ios
```
